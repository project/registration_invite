INTRODUCTION
------------

The Registration Invite module allows site administrators to configure new user 
registrations only through invitations received by emails.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/registration_invite


REQUIREMENTS
------------

This module requires the following modules:

 * Invite (https://www.drupal.org/project/invite)
 * User Referral (https://www.drupal.org/project/referral)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
------------------------------

 * The administrative user interface can be found at 
   http://yoursite.com/admin/config/people/accounts
 * Under Registration and cancellation section, select "New user registration by invitation only."

